import React from 'react';

const Blog = props => {
    return (
        <main>
            {/* POST DESTAQUE (MAIS RECENTE) */}
            <section className='container esp-top'>
                <img className='sombra-post' src='img/row-top.jpg' alt='Linha' />
                <article className='row'>
                    <div className='col-md-7 post-principal'>
                        <img src='/img/blog-post.jpg' alt='Post principal' />
                    </div>
                    <div className='col-md-5'>
                        <article className='post-principal-content'>  
                            <h1>Título do post principal</h1>    
                            <p className='autor-blog'>Por <strong>Usuário</strong> | 26 de fevereiro de 2019 </p>
                            <p>
                                Dolor sit amet, consectetur adipiscing elit. Suspendisse luctus a quam non sollicitudin. Aliquam porttitor porta tempus.
                                Aliquam quis tincidunt lorem, id faucibus nibh. Pellentesque mattis viverra tortor, ac vulputate metus lobortis eget.
                            </p>  
                            <a href='#'>Continuar lendo</a> 
                        </article>
                    </div>
                </article>
                <img className='sombra-post' src='img/row-bot.jpg' alt='Linha' />
            </section>

            {/* BLOG POSTS */}
            <section className='esp-top solucoes container'>
                <div className='row'>
                    <article className='post col-md-4'>
                        <img src='/img/post-a.jpg' alt='Imagem do post'/>
                        <div className='content gray'>
                            <h3>Título</h3>
                            <p>Garanta melhores resultados à sua IES no Enade através de diagnósticos precisos e ganho de eficiência.</p>
                            <a href='/blog'>Continuar lendo</a>
                        </div>
                    </article>
                    <article className='post col-md-4'>
                        <img src='/img/post-b.jpg' alt='Imagem do post' />
                        <div className='content gray'>
                            <h3>Título</h3>
                            <p>Garanta melhores resultados à sua IES no Enade através de diagnósticos precisos e ganho de eficiência.</p>
                            <a href='/blog'>Continuar lendo</a>
                        </div>
                    </article>
                    <article className='post col-md-4'>
                        <img src='/img/post-c.jpg' alt='Imagem do post' />
                        <div className='content gray'>
                            <h3>Título</h3>
                            <p>Garanta melhores resultados à sua IES no Enade através de diagnósticos precisos e ganho de eficiência.</p>
                            <a href='/blog'>Continuar lendo</a>
                        </div>
                    </article>
                </div>


                {/* PAGINACAO (POSTS) */}
                <nav className='paginacao'>
                    <ul className='pagination justify-content-center'>
                        <li className='page-item'><a className='page-link' href='#' aria-label='Ant'><span aria-hidden='true'>&laquo;</span></a></li>
                        <li className='page-item'><a className='page-link' href='#'>1</a></li>
                        <li className='page-item'><a className='page-link' href='#'>2</a></li>
                        <li className='page-item'><a className='page-link' href='#'>3</a></li>
                        <li className='page-item'><a className='page-link' href='#' aria-label='Pro'><span aria-hidden='true'>&raquo;</span></a></li>
                    </ul>
                </nav>
            </section>
        </main>        
    )
}
export default Blog;